FROM registry.gitlab.com/elsmmtaa_repos/nodevboxdockerimage/image:latest

USER root

COPY tools\* /root

RUN tar -xvf /root/jdk-8u131-linux-x64.tar.gz

ENV ANDROID_HOME=/root

ENV PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools

RUN cd /root/nodeServer \
    && npm install

EXPOSE 8080

CMD ["npm","start"]
